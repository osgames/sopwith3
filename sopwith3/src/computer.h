/*
    Sopwith 3
    Copyright (C) 1984-2000  David L. Clark
    Copyright (C) 1999-2001  Andrew Jenner
    Copyright (C) 2001-2003  Jornand de Buisonj�

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifndef SOPWITH_COMPUTER_H
#define SOPWITH_COMPUTER_H

#include "def.h"
#include "plane.h"

class Computerplane : public Plane {
public:
  Computerplane(int runwayx,int colour,bool runwayleft,bool unlimitedterr,int left_terr,int right_terr,bool infinitefuel,bool infiniteammo,bool infinitebombs);
  virtual bool update();
  void pilot();
  void attack(Object* target);
  void cruise();
  void alert(Object* obj);
protected:
  bool unlimitedterr;
  int left_terr;
  int right_terr;
  Object* nearobject;
};

#endif /* SOPWITH_COMPUTER_H */

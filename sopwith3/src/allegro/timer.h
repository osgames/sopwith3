/*
    Sopwith 3
    Copyright (C) 1984-2000  David L. Clark
    Copyright (C) 1999-2001  Andrew Jenner
    Copyright (C) 2001-2003  Jornand de Buisonj�

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifndef SOPWITH_ALLEGRO_TIMER_H
#define SOPWITH_ALLEGRO_TIMER_H

#include <allegro.h>

namespace {
  volatile unsigned long timertick=0;

  void timerint()
  {
    ++timertick;
  }
  END_OF_FUNCTION(timerint);
}

void inittimer()
{
  LOCK_FUNCTION(timerint);
  LOCK_VARIABLE(timertick);

  install_timer();
  install_int_ex(timerint,clockTicksPerTicks);
}

unsigned long timer()
{
  return timertick;
}

void deinittimer()
{
  remove_timer();
}

#endif /* SOPWITH_ALLEGRO_TIMER_H */

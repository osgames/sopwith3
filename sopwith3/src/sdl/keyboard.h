/*
    Sopwith 3
    Copyright (C) 1984-2000  David L. Clark
    Copyright (C) 1999-2001  Andrew Jenner
    Copyright (C) 2001-2003  Jornand de Buisonj�

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifndef SOPWITH_SDL_KEYBOARD_H
#define SOPWITH_SDL_KEYBOARD_H

#include <SDL/SDL.h>
#include "event.h"

volatile bool breakf;
int keysprev;
volatile int keysnext;
volatile int keyspressed;

int inkey()
{
  pollEvents();
  for (std::list<SDL_Event>::iterator event=events.begin();event!=events_end;++event) {
    switch(event->type) {
      case SDL_KEYDOWN:
        if (event->key.keysym.sym>=1 && event->key.keysym.sym<=0xff) {
          events.erase(event);
          return event->key.keysym.sym;
        }
      default:
        break;
    }
  }
  return 0;
}

void flushkeybuf()
{
  pollEvents();
  for (std::list<SDL_Event>::iterator event=events.begin();event!=events_end;) {
    bool erase=true;
    switch(event->type) {
      case SDL_KEYDOWN:
      case SDL_KEYUP:
      default:
        erase=false;
        break;
    }
    if (erase)
      event=events.erase(event);
    else
      ++event;
  }
}

void pollkeyboard()
{
  int k;
  if (!ibmkeyboard)
    return;
  pollEvents();
  for (std::list<SDL_Event>::iterator event=events.begin();event!=events_end;) {
    bool erase=true;
    switch(event->type) {
      case SDL_KEYDOWN:
      case SDL_KEYUP:
        switch (event->key.keysym.sym) {
          case SDLK_x:         k=KEY_ACCEL;     break;
          case SDLK_z:         k=KEY_BRAKE;     break;
          case SDLK_COMMA:     k=KEY_CLIMB;     break;
          case SDLK_SLASH:     k=KEY_DESCEND;   break;
          case SDLK_PERIOD:    k=KEY_FLIP;      break;
          case SDLK_SPACE:     k=KEY_FIRE;      break;
          case SDLK_b:         k=KEY_BOMB;      break;
          case SDLK_h:         k=KEY_GOHOME;    break;
          case SDLK_s:         k=KEY_SOUND;     break;
          case SDLK_SCROLLOCK: k=KEY_BREAK;     breakf=true; break;
          case SDLK_p:
            k=KEY_PAUSEGAME;
            break;
          default: k=0;
        }
        if (k!=0)
          if (event->type==SDL_KEYUP) {
            if ((k&keysprev)!=0)
              keysnext&=~k;
            keyspressed&=~k;
          }
          else {
            keyspressed|=k;
            keysnext|=k;
          }
        break;
      default:
        erase=false;
        break;
    }
    if (erase)
      event=events.erase(event);
    else
      ++event;
  }
}

void initkeyboard()
{
}

void deinitkeyboard()
{
}

#endif /* SOPWITH_SDL_KEYBOARD_H */

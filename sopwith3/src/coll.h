/*
    Sopwith 3
    Copyright (C) 1984-2000  David L. Clark
    Copyright (C) 1999-2001  Andrew Jenner
    Copyright (C) 2001-2003  Jornand de Buisonj�

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifndef SOPWITH_COLL_H
#define SOPWITH_COLL_H

extern unsigned char bombspritescoll[0x8][0x8];
extern unsigned char targetspritescoll[0x4][0x20];
extern unsigned char debrisspritescoll[0x20];
extern unsigned char fragspritescoll[0x3][0x8][0x8];
extern unsigned char planespritescoll[0x2][0x10][0x20];
extern unsigned char finalespritescoll[0x4][0x20];
extern unsigned char fallingspritescoll[0x2][0x20];
extern unsigned char flockspritescoll[0x2][0x20];
extern unsigned char birdspritescoll[0x2][0x2];
extern unsigned char oxspritescoll[0x2][0x20];
extern unsigned char missilespritescoll[0x10][0x8];
extern unsigned char starburstspritescoll[0x2][0x8];
extern unsigned char pixelspritescoll[0x1];

#endif /* SOPWITH_COLL_H */

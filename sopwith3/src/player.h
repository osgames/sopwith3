/*
    Sopwith 3
    Copyright (C) 1984-2000  David L. Clark
    Copyright (C) 1999-2001  Andrew Jenner
    Copyright (C) 2001-2003  Jornand de Buisonj�

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifndef SOPWITH_PLAYER_H
#define SOPWITH_PLAYER_H

#include "def.h"
#include "plane.h"

class Playerplane : public Plane {
public:
  Playerplane(int runwayx,int colour,bool runwayleft,bool unlimitedlives,int maxlives,bool infinitefuel,bool infiniteammo,bool infinitebombs);
  virtual void init();
  virtual void reinit();
  virtual bool update();
  virtual void processkeys(int keys);
  virtual void refuel();
  virtual void burnfuel();
protected:
  bool unlimitedlives;
};

extern Playerplane* player;

int inkeys();

#endif /* SOPWITH_PLAYER_H */
